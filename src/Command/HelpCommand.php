<?php

	namespace Delta\Console\Command {

		use Delta\Console\Command;
		use Delta\Console\Definition\Argument;

		/**
		 * Class HelpCommand
		 * The command invoked when application is called with
		 * help, --usage or --help
		 *
		 * @package Delta\Console\Command
		 */
		class HelpCommand extends Command
		{
			/**
			 * {@inheritdoc}
			 */
			public function isSystem()
			{
				return true;
			}

			/**
			 * {@inheritdoc}
			 */
			public function configure()
			{
				$this
					->setName('help')
					->setHelp('Display global or command-specific help message')
					->addArgument('command', Argument::OPTIONAL)
				;
			}

			/**
			 * {@inheritdoc}
			 */
			public function execute()
			{
				$command = $this->getArgument('command');

				$usage = null === $command ? $this->application->getUsage() : $this->application->get($command)->getUsage();

				$this->write($usage);

				return 0;
			}
			
		}

	}

?>