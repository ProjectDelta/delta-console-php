<?php

	namespace Delta\Console\Command {

		use Delta\Console\Command;
		use Delta\Console\Definition\Option;

		/**
		 * Class VersionCommand
		 *
		 * @package Delta\Console\Command
		 */
		class VersionCommand extends Command
		{
			/**
			 * {@inheritdoc}
			 */
			public function isSystem()
			{
				return true;
			}

			/**
			 * {@inheritdoc}
			 */
			public function configure()
			{
				$this
					->setName('version')
					->setHelp('Display application version and exit')
					->addOption(
						'raw',
						null,
						Option::FLAG,
						'Output version as raw plain text'
					)
					;
			}

			/**
			 * {@inheritdoc}
			 */
			public function execute()
			{
				$raw = $this->getOption('raw');
				$version = $this->getApplication()->getVersion();
				$name = $this->getApplication()->getName();
				$message = $raw ? $version : sprintf('<strong>%s</strong> version <strong>%s</strong>', $name, $version);
				$this->write($message);

				return 0;
			}
			
		}

	}

?>