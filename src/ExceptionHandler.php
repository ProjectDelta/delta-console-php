<?php

/**
 * This file is part of the delta/console library
 * (c) Yannoff (https://github.com/yannoff)
 *
 * @project   delta/console
 * @link      https://github.com/delta/console
 * @license   https://github.com/delta/console/blob/master/LICENSE
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Delta\Console;

use Exception;
use Delta\Console\Exception\Definition\MissingArgumentsException;
use Delta\Console\IO\StreamAware;

/**
 * Class ExceptionHandler
 * Handler for all kinds of exceptions
 *
 * @package Delta\Console
 */
class ExceptionHandler
{
    /**
     * Handler method for exceptions
     *
     * @param Exception   $exception The thrown exception
     * @param StreamAware $callee    The object calling this method
     *
     * @return int The exception status code
     */
    public static function process(Exception $exception, StreamAware $callee)
    {
        $type = get_class($exception);

        switch ($type):
            case MissingArgumentsException::class:
                /** @var MissingArgumentsException $exception */
                foreach ($exception->getArguments() as $arg) {
                    $callee->error("Error: Mandatory argument '{$arg}' is missing");
                }
                break;

            default:
                $callee->error((string) $exception);
                break;
        endswitch;

        $callee->error('Exiting.');

        return $exception->getCode();
    }
}
