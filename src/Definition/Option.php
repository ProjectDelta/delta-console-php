<?php

namespace Delta\Console\Definition;

use Delta\Console\Application;
use Delta\Console\Exception\Definition\InvalidOptionTypeException;
use Delta\Console\IO\ASCII;

/**
 * Class Option
 * Option definition item
 *
 * @package Delta\Console\Definition
 */
class Option extends Item
{
    /**
     * Option type: value
     *
     * @var int
     */
    const VALUE = 10;

    /**
     * Option type: flag
     *
     * @var int
     */
    const FLAG = 20;

    /**
     * Option type: array (ie: multiple values)
     *
     * @var int
     */
    const MULTI = 40;

    /**
     * Optional short name for the option
     *
     * @var string|null
     */
    protected $short;

    /**
     * Option constructor.
     *
     * @param string $name    Option name. This is also the name used as a key in the Definition registry
     * @param string $short   Optional short name for the option
     * @param int    $type    Type of option: value or flag
     * @param string $help    Description text for the option
     * @param mixed  $default Optional default value for the option
     *
     * @throws InvalidOptionTypeException
     */
    public function __construct($name, $short = null, $type = self::FLAG, $help = '', $default = null)
    {
        if (!Option::isValidType($type)) {
            throw new InvalidOptionTypeException($name, $type);
        }

        $this->name = $name;
        $this->short = $short;
        $this->type = $type;
        $this->help = $help;
        // Override default value for flags => false
        if (self::FLAG === $type) {
            $default = false;
        }
        $this->default = $default;
    }

    /**
     * Getter for the option's short name
     *
     * @return string|null
     */
    public function getShortcut()
    {
        return $this->short;
    }

    /**
     * True if the option is typed as a flag
     *
     * @return bool
     */
    public function isFlag()
    {
        return (self::FLAG === $this->type);
    }

    /**
     * True if the option is typed as a value
     *
     * @return bool
     */
    public function isValue()
    {
        return (self::VALUE === $this->type);
    }

    /**
     * {@inheritdoc}
     */
    public static function getValidTypes()
    {
        return [ self::VALUE, self::FLAG, self::MULTI ];
    }

    /**
     * {@inheritdoc}
     */
    public function getSynopsis($tab = ASCII::TAB, $width = Application::PAD)
    {
        $synopsis = implode(', ', $this->getNames());

        if ($this->isValue()) {
            $synopsis .= sprintf(' %s', 'VALUE');
        }

        $help = sprintf("%s%-{$width}s %s", $tab, $synopsis, $this->help);

        if ($this->hasDefault()) {
            $help .= sprintf(' (default: <strong>%s</strong>)', $this->default);
        }

        return $help;
    }

    /**
     * Get the option invocation names (short & long)
     *
     * @return array
     */
    protected function getNames()
    {
        $names = [];

        if (!empty($this->short)) {
            $names[] = sprintf('-%s', $this->short);
        }

        $names[] = sprintf('--%s', $this->name);

        return $names;
    }
}
