<?php

namespace Delta\Console\Exception;

/**
 * Interface ResolverException
 *
 * Contract for exceptions raised by the argument resolver
 *
 * @package Delta\Console\Exception
 */
interface ResolverException
{
}
