<?php

namespace Delta\Console\Exception;

/**
 * Interface DefinitionException
 *
 * Contract for exceptions related to Definition issues
 *
 * @package Delta\Console\Exception
 */
interface DefinitionException
{
}
