<?php

namespace Delta\Console\Exception;

/**
 * Interface CommandException
 *
 * Contract for command-related exceptions
 *
 * @package Delta\Console\Exception
 */
interface CommandException
{
}
