<?php

namespace Delta\Console\Exception;

use Exception;
use RuntimeException as BaseRuntimeException;

/**
 * Class RuntimeException
 *
 * Base class for exceptions raised by an application user mistaken usage
 *
 * @package Delta\Console\Exception
 */
class RuntimeException extends BaseRuntimeException
{
    /**
     * RuntimeException constructor.
     *
     * @param string         $message  Error message
     * @param int            $code     Error status code to be sent to the terminal (defaults to 1)
     * @param Exception|null $previous Optional parent in exception chaining
     */
    public function __construct($message = "", $code = 1, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * Default string cast-type formatter for RuntimeExceptions
     *
     * @return string
     */
    public function __toString()
    {
        return sprintf('Error: %s (code: %s)', $this->message, $this->code);
    }
}
