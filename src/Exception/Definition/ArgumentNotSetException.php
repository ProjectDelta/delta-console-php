<?php

namespace Delta\Console\Exception\Definition;

use Exception;
use Delta\Console\Exception\ResolverException;
use Delta\Console\Exception\RuntimeException;

/**
 * Class ArgumentNotSetException
 *
 * Thrown by ArgvResolver when trying to get the value of an argument that was not supplied by the user and has no default value
 *
 * @package Delta\Console\Exception\Definition
 */
class ArgumentNotSetException extends RuntimeException implements ResolverException
{
    /**
     * ArgumentNotSetException constructor.
     *
     * @param string         $name     Name of the queried argument
     * @param int            $code     Error status code to be sent to the terminal (defaults to 128)
     * @param Exception|null $previous Optional parent exception
     */
    public function __construct($name = "", $code = 128, Exception $previous = null)
    {
        $message = sprintf('Argument "%s" was not supplied and has no default value', $name);
        parent::__construct($message, $code, $previous);
    }
}
