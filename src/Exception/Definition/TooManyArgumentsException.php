<?php

namespace Delta\Console\Exception\Definition;

use Exception;
use Delta\Console\Exception\ResolverException;
use Delta\Console\Exception\RuntimeException;

/**
 * Class TooManyArgumentsException
 *
 * Thrown when the command is invoked with an excessive number of arguments
 *
 * @package Delta\Console\Exception\Definition
 */
class TooManyArgumentsException extends RuntimeException implements ResolverException
{
    /**
     * TooManyArgumentsException constructor.
     *
     * @param mixed          $value    Overflowing argument value
     * @param int            $pos      Overflowing argument position
     * @param int            $max      Total number of defined arguments
     * @param int            $code     Error status code to be sent to the terminal (defaults to 128)
     * @param Exception|null $previous Optional parent exception
     */
    public function __construct($value, $pos, $max, $code = 128, Exception $previous = null)
    {
        $message = sprintf('Too many arguments (%s): max: %s, given: %s', $value, $max, $pos);
        parent::__construct($message, $code, $previous);
    }
}
