<?php

namespace Delta\Console\Exception\Definition;

use Exception;
use Delta\Console\Exception\DefinitionException;
use Delta\Console\Exception\LogicException;

/**
 * Class UndefinedOptionException
 *
 * Thrown when an undefined option is queried via Definition::getOption()
 *
 * @package Delta\Console\Exception\Definition
 */
class UndefinedOptionException extends LogicException implements DefinitionException
{
    /**
     * UndefinedOptionException constructor.
     *
     * @param string         $name     Name of the queried option
     * @param int            $code     Error status code to be sent to the terminal (defaults to 128)
     * @param Exception|null $previous Optional parent exception
     */
    public function __construct($name = "", $code = 128, Exception $previous = null)
    {
        $message = sprintf('Queried option "%s" is not defined', $name);
        parent::__construct($message, $code, $previous);
    }
}
