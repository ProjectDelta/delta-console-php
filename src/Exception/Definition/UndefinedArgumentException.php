<?php

namespace Delta\Console\Exception\Definition;

use Exception;
use Delta\Console\Exception\DefinitionException;
use Delta\Console\Exception\LogicException;

/**
 * Class UndefinedArgumentException
 *
 * Thrown when an undefined argument is queried via Definition::getArgument()
 *
 * @package Delta\Console\Exception\Definition
 */
class UndefinedArgumentException extends LogicException implements DefinitionException
{
    /**
     * UndefinedArgumentException constructor.
     *
     * @param string         $name     Name of the queried argument
     * @param int            $code     Error status code to be sent to the terminal (defaults to 128)
     * @param Exception|null $previous Optional parent exception
     */
    public function __construct($name = "", $code = 128, Exception $previous = null)
    {
        $message = sprintf('Queried argument "%s" is not defined', $name);
        parent::__construct($message, $code, $previous);
    }
}
