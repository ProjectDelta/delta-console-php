<?php

namespace Delta\Console\Exception;

use Exception;

/**
 * Class LogicException
 *
 * Base class for exceptions raised by a misconception in the command or application code
 *
 * @package Delta\Console\Exception
 */
class LogicException extends FatalException
{
    /**
     * LogicException constructor.
     *
     * @param string         $message  Error message
     * @param int            $code     Error status code to be sent to the terminal (defaults to 1)
     * @param Exception|null $previous Optional parent in exception chaining
     */
    public function __construct($message = "", $code = 1, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
