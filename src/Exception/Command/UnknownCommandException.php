<?php

namespace Delta\Console\Exception\Command;

use Exception;
use Delta\Console\Exception\CommandException;
use Delta\Console\Exception\RuntimeException;

/**
 * Class UnknownCommandException
 *
 * Thrown when the application script is invoked with a command name unknown to the registry
 *
 * @package Delta\Console\Exception\Command
 */
class UnknownCommandException extends RuntimeException implements CommandException
{
    /**
     * UnknownCommandException constructor.
     *
     * @param string         $name     Name of the queried command
     * @param int            $code     Error status code to be sent to the terminal (defaults to 127)
     * @param Exception|null $previous Optional parent exception
     */
    public function __construct($name = "", $code = 127, Exception $previous = null)
    {
        $message = sprintf('Command with name "%s" not found by the application', $name);
        parent::__construct($message, $code, $previous);
    }
}
