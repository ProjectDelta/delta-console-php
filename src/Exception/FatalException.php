<?php

/**
 * This file is part of the delta/console library
 * (c) Yannoff (https://github.com/yannoff)
 *
 * @project   delta/console
 * @link      https://github.com/delta/console
 * @license   https://github.com/delta/console/blob/master/LICENSE
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Delta\Console\Exception;

/**
 * Class FatalException
 * Represent a blocking exception: when raised code execution should terminate
 *
 * @package Delta\Console\Exception
 */
class FatalException extends RuntimeException
{
    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return sprintf('Fatal: %s (code: %s)', $this->message, $this->code);
    }
}
