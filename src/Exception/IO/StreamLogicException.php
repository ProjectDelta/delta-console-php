<?php

namespace Delta\Console\Exception\IO;

use Delta\Console\Exception\LogicException;

/**
 * Class StreamLogicException
 *
 * Generic exception for stream-related logic inconsistencies
 *
 * @package Delta\Console\Exception\IO
 */
class StreamLogicException extends LogicException
{
}
