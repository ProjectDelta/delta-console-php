<?php

namespace Delta\Console\Exception\IO;

use Delta\Console\Exception\LogicException;

/**
 * Class BadTagChainException
 *
 * Thrown by PosixFormatter::format() when tags precedence
 * is not respected
 *
 * Eg: "<strong>My bad<blue>chain</strong> text</blue>"
 *
 * @package Delta\Console\Exception\IO
 */
class BadTagChainException extends LogicException
{
}
