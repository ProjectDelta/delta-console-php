<?php

namespace Delta\Console\Exception\IO;

use Delta\Console\Exception\LogicException;

/**
 * Class TagMismatchException
 *
 * Thrown by PosixFormatter::format() when trying to
 * close a tag that was not previously opened
 *
 * Eg: "This is my text</strong> with a mismatch"
 *
 * @package Delta\Console\Exception\IO
 */
class TagMismatchException extends LogicException
{
}
