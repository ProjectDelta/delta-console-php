<?php

namespace Delta\Console\Exception\IO;

use Exception;

/**
 * Class UnsupportedStreamException
 *
 * Thrown by StreamFactory when queried an unsupported stream short name
 *
 * @package Delta\Console\Exception\IO
 */
class UnsupportedStreamException extends StreamLogicException
{
    /**
     * UnsupportedStreamException constructor.
     *
     * @param string         $stream   Queried stream short name
     * @param int            $code     Error status code to be sent to the terminal (defaults to 1)
     * @param Exception|null $previous Optional parent exception
     */
    public function __construct($stream = "", $code = 1, Exception $previous = null)
    {
        $message = sprintf('Unsupported "%s" stream', $stream);
        parent::__construct($message, $code, $previous);
    }
}
