<?php

namespace Delta\Console\IO;

use Delta\Console\Exception\IO\UnsupportedStreamException;
use Delta\Console\IO\Stream\IOStream;
use Delta\Console\IO\Stream\StandardError;
use Delta\Console\IO\Stream\StandardInput;
use Delta\Console\IO\Stream\StandardOutput;

/**
 * Class StreamFactory
 * Factory for I/O writers & readers
 *
 * @package Delta\Console\IO
 */
class StreamFactory
{
    /**
     * Array map of the available streams, indexed by their short names
     *
     * @var array
     */
    protected static $streams = [
        IOStream::STDIN => StandardInput::class,
        IOStream::STDERR => StandardError::class,
        IOStream::STDOUT => StandardOutput::class,
    ];

    /**
     * Create a new instance of the queried stream
     *
     * @param string $name The stream's short name
     *
     * @return IOStream
     */
    public static function create($name)
    {
        if (! self::supports($name)){
            throw new UnsupportedStreamException($name);
        }

        $class = self::$streams[$name];

        return new $class();
    }

    /**
     * Validate the queried IO stream short name
     *
     * @param string $stream The stream short name
     *
     * @return bool
     */
    public static function supports($stream)
    {
        return \array_key_exists($stream, self::$streams);
    }
}
