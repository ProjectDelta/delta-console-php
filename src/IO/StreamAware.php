<?php

namespace Delta\Console\IO;

/**
 * Interface StreamAware
 * Represent objects with stream interaction capabilities
 *
 * @package Delta\Console\IO
 */
interface StreamAware
{
    /**
     * Print text to STDERR
     *
     * @param string $text   The text to print (defaults to empty string)
     * @param string $ending Ending character or text
     *
     * @return bool|int
     */
    public function error($text = '', $ending = ASCII::LF);

    /**
     * Print text to STDOUT
     *
     * @param string $text   The text to print (defaults to empty string)
     * @param string $ending Ending character or text
     *
     * @return bool|int
     */
    public function write($text = '', $ending = ASCII::LF);

    /**
     * Read contents from the standard input
     *
     * @param bool $interactive Whether to accept user input
     *
     * @return string|false The contents or **false** in case of failure
     */
    public function read($interactive = false);
}
