<?php

namespace Delta\Console\IO;

use Delta\Console\Exception\IO\UnsupportedStreamException;
use Delta\Console\IO\Stream\IOStream;

/**
 * Class StreamRegistry
 * Registry for I/O writers & readers
 *
 * @package Delta\Console\IO
 */
class StreamRegistry
{
    /**
     * @var IOStream[]
     */
    protected static $streams = [];

    /**
     * Lazy stream getter & initializer method
     *
     * @param string $stream The stream short name
     *
     * @return IOStream
     */
    public static function get($stream)
    {
        if (!self::supports($stream)) {
            throw new UnsupportedStreamException($stream);
        }

        if (\array_key_exists($stream, self::$streams)) {
            return self::$streams[$stream];
        }

        return self::add($stream);
    }

    /**
     * Create a new stream wrapper instance and store it in the registry
     *
     * @param string $stream The stream short name
     *
     * @return IOStream
     */
    public static function add($stream)
    {
        $member = StreamFactory::create($stream);

        return self::$streams[$stream] = $member;
    }

    /**
     * Validate the queried IO stream short name
     *
     * @param string $stream The stream short name
     *
     * @return bool
     */
    public static function supports($stream)
    {
        return in_array($stream, [IOStream::STDIN, IOStream::STDOUT, IOStream::STDERR]);
    }
}
