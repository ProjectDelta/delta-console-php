<?php

namespace Delta\Console\IO\Stream;

use Delta\Console\IO\Output\Formatter;

/**
 * Class StandardError
 *
 * Writer stream for standard error (STDERR)
 *
 * @package Delta\Console\IO\Stream
 */
class StandardError extends Wrapper implements IOWriter
{
    /**
     * The stream short name
     */
    const NAME = self::STDERR;

    /**
     * The stream open mode
     */
    const MODE = self::APPEND;

    /**
     * {@inheritdoc}
     */
    public function write($contents, $ending = Formatter::LF)
    {
        return fwrite($this->handle, $contents . $ending);
    }
}
