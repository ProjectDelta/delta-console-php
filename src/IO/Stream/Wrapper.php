<?php

namespace Delta\Console\IO\Stream;

use Delta\Console\Exception\IO\StreamLogicException;
use Delta\Console\Exception\UndefinedConstantException;
use Delta\Console\IO\ConstantAccessor;
use Delta\Console\IO\FStat;

/**
 * Class Wrapper
 *
 * Super-class for PHP I/O Stream wrappers (both writers & readers)
 *
 * @package Delta\Console\IO\Stream
 */
class Wrapper
{
    /**
     * Handle to the stream wrapper
     *
     * @var resource
     */
    protected $handle;

    /**
     * Wrapper constructor.
     */
    public function __construct()
    {
        $this->bind();
    }

    /**
     * Wrapper destructor
     * Free the stream handle resource
     */
    public function __destruct()
    {
        $this->free();
    }

    /**
     * Bind the wrapper stream resource
     */
    protected function bind()
    {
        $this->handle = fopen($this->getURI(), $this->getOpenMode());
    }

    /**
     * Unbind the wrapper stream resource
     */
    protected function free()
    {
        is_resource($this->handle) && fclose($this->handle);
    }

    /**
     * Build the PHP stream wrapper URI using the child class NAME constant
     *
     * @return string
     */
    public function getURI()
    {
        $wrapper = $this->constant('NAME');

        return sprintf('php://%s', $wrapper);
    }

    /**
     * Build the PHP stream wrapper fopen() mode using the child class MODE constant
     *
     * @return string
     */
    public function getOpenMode()
    {
        return $this->constant('MODE');
    }

    /**
     * Check whether the stream is a FIFO
     *
     * @return bool
     */
    public function isPiped()
    {
        return FStat::isFifo($this->handle);
    }

    /**
     * Check whether the stream is a regular file redirect
     *
     * @return bool
     */
    public function isFile()
    {
        return FStat::isRegularFile($this->handle);
    }

    /**
     * Generic getter for Wrapper child classes constants
     *
     * @param string $name The class constant name
     *
     * @return mixed
     * @throws StreamLogicException If the queried constant is not defined
     */
    protected function constant($name = '')
    {
        try {
            return ConstantAccessor::get($name, static::class);
        } catch (UndefinedConstantException $e) {
            $message = sprintf('Classes extending "%s" must have the "%s" constant defined', __CLASS__, $e->getName());
            throw new StreamLogicException($message);
        }
    }
}
