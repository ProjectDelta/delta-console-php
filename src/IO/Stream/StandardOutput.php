<?php

namespace Delta\Console\IO\Stream;

use Delta\Console\IO\Output\Formatter;

/**
 * Class StandardOutput
 *
 * Writer stream for standard output (STDOUT)
 *
 * @package Delta\Console\IO\Stream
 */
class StandardOutput extends Wrapper implements IOWriter
{
    /**
     * The stream short name
     */
    const NAME = self::STDOUT;

    /**
     * The stream open mode
     */
    const MODE = self::APPEND;

    /**
     * {@inheritdoc}
     */
    public function write($contents, $ending = Formatter::LF)
    {
        return fwrite($this->handle, $contents . $ending);
    }
}
