<?php

namespace Delta\Console\IO\Stream;

/**
 * Interface IOStream
 * Contract for both types of I/O streams: readers & writers
 *
 * @package Delta\Console\IO\Stream
 */
interface IOStream
{
    /**
     * Supported streams short names
     *
     * @var string
     */
    const STDIN = 'stdin';
    const STDOUT = 'stdout';
    const STDERR = 'stderr';

    /**
     * Available modes for fopen()
     *
     * @var string
     */
    const APPEND = 'a+';
    const READONLY = 'r';
}
