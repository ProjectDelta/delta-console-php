<?php

namespace Delta\Console\IO\Stream;

use Delta\Console\IO\Output\Formatter;

/**
 * Interface IOWriter
 * Contract for I/O writer streams
 *
 * @package Delta\Console\IO\Stream
 */
interface IOWriter extends IOStream
{
    /**
     * Write contents to the output stream
     *
     * @param string $contents The contents to write
     * @param string $ending   Character or text to be appended (defaults to "\n")
     *
     * @return int|false Number of bytes written or **false** on error
     */
    public function write($contents, $ending = Formatter::LF);
}
