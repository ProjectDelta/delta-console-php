<?php

namespace Delta\Console\IO\Stream;

/**
 * Class StandardInput
 *
 * Reader stream for standard input (STDIN)
 *
 * @package Delta\Console\IO\Stream
 */
class StandardInput extends Wrapper implements IOReader
{
    /**
     * The stream short name
     */
    const NAME = self::STDIN;

    /**
     * The stream open mode
     */
    const MODE = self::READONLY;

    /**
     * {@inheritdoc}
     */
    public function read($interactive = false)
    {
        // Unless explicitly forced using the $interactive parameter,
        // assume we only want to read from stdin when it's a piped
        // input or a regular file redirect
        if ($interactive || $this->isPiped() || $this->isFile()) {
            return stream_get_contents($this->handle);
        }

        return '';
    }
}
