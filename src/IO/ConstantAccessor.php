<?php

namespace Delta\Console\IO;

use Delta\Console\Exception\UndefinedConstantException;

/**
 * Class ConstantAccessor
 *
 * Allow access to class constants
 *
 * @package Delta\Console\IO
 */
class ConstantAccessor
{
    /**
     * Generic getter for class constants
     *
     * @param string $name   Name of the class constant
     * @param string $target Target class (defaults to *static*)
     *
     * @return mixed
     * @throws UndefinedConstantException If the queried class constant is not defined
     */
    public static function get($name = '', $target = 'static')
    {
        $constant = sprintf('%s::%s', $target, $name);

        if (!defined($constant)) {
            throw new UndefinedConstantException($name, $target);
        }

        return constant($constant);
    }
}
