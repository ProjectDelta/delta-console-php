<?php

namespace Delta\Console\IO;

/**
 * Class ASCII
 * ASCII character/sequence representation constants
 *
 * @package Delta\Console\IO
 */
class ASCII
{
    /**
     * New line string representations
     *
     * @var string
     */
    const LF = "\n";
    const CR = "\r";
    const CRLF = "\r\n";

    /**
     * Tabulation character
     *
     * @var string
     */
    const TAB = "\t";

    /**
     * Soft-tab string: 4 spaces
     *
     * @var string
     */
    const STAB = "    ";
}
