<?php

namespace Delta\Console\IO\Output;

/**
 * Class Verbosity
 * Handle application-wide verbosity level
 *
 * @package Delta\Console\IO\Output
 */
class Verbosity
{
    /**
     * Default log level
     * Messages of this level are ALWAYS displayed
     *
     * @var int
     */
    const ERROR = 0;

    /**
     * @var int
     */
    const WARN = 8;

    /**
     * @var int
     */
    const INFO = 16;

    /**
     * @var int
     */
    const DEBUG = 32;

    /**
     * Current verbosity level
     *
     * @var int
     */
    public static $level = 0;

    /**
     * Getter for the current verbosity level
     *
     * @return int
     */
    public static function get()
    {
        return self::$level;
    }

    /**
     * Setter for the current verbosity level
     *
     * @param int $level
     */
    public static function set($level)
    {
        self::$level = $level;
    }
}

