<?php

namespace Delta\Console\IO\Output;

/**
 * Class CygwinFormatter
 * Cygwin/GitBash Flavored terminal formatter (Not implemented yet)
 *
 * @package Delta\Console\IO\Output
 */
class CygwinFormatter extends PosixFormatter
{
    // TODO Implement specific formatting logic
}
