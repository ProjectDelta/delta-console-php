<?php

namespace Delta\Console\IO\Output;

use Delta\Console\IO\Output\Posix\TagMap;

/**
 * Class Token
 *
 * Object abstraction for the PosixFormatter tokens
 *
 * @package Delta\Console\IO\Output
 */
class Token
{
    /**
     * Plain-text token contents
     *
     * @var string
     */
    public $raw;

    /**
     * @param string $raw The plain-text token contents
     */
    public function __construct($raw = null)
    {
        $this->raw = $raw;
    }

    /**
     * @return string The string representation of the token
     */
    public function __toString()
    {
        return $this->raw;
    }

    /**
     * @return bool
     */
    public function isTag()
    {
        $spaceless = \ltrim($this->raw);
        $trimmed = \trim($spaceless, '></');

        return \preg_match('/^</', $spaceless) && TagMap::has($trimmed);
    }

    /**
     * @return bool
     */
    public function isText()
    {
        return (! $this->isTag());
    }
}
