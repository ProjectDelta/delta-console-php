<?php

namespace Delta\Console\IO\Output;

use Delta\Console\Application;
use Delta\Console\IO\ASCII;

/**
 * Interface Formatter
 * Contract for console output formatters
 *
 * @package Delta\Console\IO\Output
 */
interface Formatter
{
    /**
     * Supported Operating Systems
     *
     * @var string
     */
    const OS_LINUX = 'Linux';
    const OS_DARWIN = 'Darwin';
    const OS_CYGWIN = 'Cygwin';

    /**
     * New line characters or strings
     *
     * @deprecated Use ASCII constants instead
     *
     * @var string
     */
    const LF = ASCII::LF;
    const CR = ASCII::CR;
    const CRLF = ASCII::CRLF;

    /**
     * Tabulation character
     *
     * @deprecated Use ASCII constant instead
     *
     * @var string
     */
    const TAB = ASCII::TAB;

    /**
     * Soft-tab string: 4 spaces
     *
     * @deprecated Use ASCII constant instead
     *
     * @var string
     */
    const STAB = ASCII::STAB;

    /**
     * Minimal left-padding width for the name columns in help
     *
     * @deprecated Use Application constant instead
     *
     * @var string
     */
    const PAD = Application::PAD;

    /**
     * Render the given markup text into a terminal-compatible format
     *
     * @param string $text The pre-formatted text to be rendered
     *
     * @return string
     */
    public function format($text);
}
