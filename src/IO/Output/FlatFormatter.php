<?php

namespace Delta\Console\IO\Output;

/**
 * Class FlatFormatter
 * The fallback formatter used in case of non-compatible OS
 * Strip all tags and output raw text
 *
 * @package Delta\Console\IO\Output
 */
class FlatFormatter implements Formatter
{
    /**
     * {@inheritdoc}
     */
    public function format($text)
    {
        return \strip_tags($text);
    }
}
