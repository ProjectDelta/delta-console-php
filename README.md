# delta/console

A simple, lightweight console implementation for command-line PHP applications.


## Why are we here ?

This library was conceived as an alternative to the [symfony/console](https://github.com/symfony/console) component.

Truth is, the symfony component may be good for rapid-application-development or for a proof-of-concept, but on the other hand doesn't seem to be the best option for most use-cases, since only a few among the plenty of available features are necessary.

Yet the idea was not to [reinvent the wheel](https://sourcemaking.com/antipatterns/reinvent-the-wheel), but merely to provide
a simpler, lighter (in size & resource) implementation for PHP command-line applications.


